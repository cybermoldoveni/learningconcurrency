//
//  main.cpp
//  LearningConcurrency
//
//  Created by Denis Grigor on 2/6/2014.
//  Copyright (c) 2014 CyberMoldoveni. All rights reserved.
//

#include <iostream>
#include "Accounting.h"
#include <thread>

void sayHello()
{
    std::cout << "Hello from a thread"<<std::endl;
}


int main(int argc, const char * argv[])
{
    std::cout << "Starting the programm\n";
    AccountTestCase();
    
    std::thread t(sayHello);
    t.join();
    
    std::cout << "Starting the programm\n";
    return 0;
}

