//
//  Accounting.cpp
//  LearningConcurrency
//
//  Created by Denis Grigor on 2/6/2014.
//  based on Java code by Malek Barhoush, mbarhoush@hotmail.com;
//   * $Revision: 1.0 $
//   * $Last Revision Date: 2014/01/02
//
//


#include "Accounting.h"
#include <thread>

void AccountTestCase()
{
    Client* account[10] = {
        new Client(1234,"Mike",1000),
        new Client(2345,"Adam",2000),
        new Client(3456,"Linda",3000),
        new Client(4567,"John",4000),
        new Client(5678,"Rami",5000),
        new Client(6789,"Lee",6000),
        new Client(7890,"Tom",7000),
        new Client(8901,"Lisa",8000),
        new Client(9012,"Sam",9000),
        new Client(4321,"Ted",10000)
    };
    Depositor* deposit[10] = {
        new Depositor(*account[0]),
        new Depositor(*account[1]),
        new Depositor(*account[2]),
        new Depositor(*account[3]),
        new Depositor(*account[4]),
        new Depositor(*account[5]),
        new Depositor(*account[6]),
        new Depositor(*account[7]),
        new Depositor(*account[8]),
        new Depositor(*account[9]),
    };
    Withdrawer* withdraw[10] = {
        new Withdrawer(*account[0]),
        new Withdrawer(*account[1]),
        new Withdrawer(*account[2]),
        new Withdrawer(*account[3]),
        new Withdrawer(*account[4]),
        new Withdrawer(*account[5]),
        new Withdrawer(*account[6]),
        new Withdrawer(*account[7]),
        new Withdrawer(*account[8]),
        new Withdrawer(*account[9]),
    };
    
    std::cout<<"Print initial account balances:"<<std::endl;
    // Print initial account balances
    for(int i=0;i<10;i++) {std::cout << *account[i];}
    
    std::cout<<"Depositor and Withdrawal threads have been created:"<<std::endl;

    for(int i=0; i<10; i++)
    {
        deposit[i]->run();
        withdraw[i]->run();
    }

    std::cout<<"Print initial account balances:"<<std::endl;
    // Print initial account balances
    for(int i=0;i<10;i++) {std::cout << *account[i];}



}

/* IMPLEMENTATIONS */

std::ostream & operator<<(std::ostream &os, const Client &ciudik)
{
    os << "Account: " <<ciudik.account_number<<" \tName: "<<std::left<<std::setw(6)<<ciudik.client_name<<" \tBalance: "<<std::setw(10)<<ciudik.balance<<std::endl;
    return os;
}


Client::Client(int number, std::string name, double balance)
    :account_number(number), client_name(name), balance(balance)
{
    
}

void Client::deposit(double amount)
{
    // Waste some time doing fake computations
    // do not remove or modify any of the following 3 statements
    double k = 999999999;
    for(int i=0;i<100;i++)
        k = k / 2;
    
    balance = balance + amount;
    
    // Waste some time doing fake computations
    // do not remove or modify any of the following 3 statements
    k = 999999999;
    for(int i=0;i<100;i++)
        k = k / 2;
}

void Client::withdraw(double amount){
    
    // Waste some time doing fake computations
    // do not remove or modify any of the following 3 statements
    double k = 999999999;
    for(int i=0;i<100;i++)
        k = k / 2;
    
    balance = balance - amount;
    
    // Waste some time doing fake computations
    // do not remove or modify any of the following 3 statements
    k = 999999999;
    for(int i=0;i<100;i++)
        k = k / 2;
}

Depositor::Depositor(Client this_client)
:client_account(this_client)
{
    
}

void Depositor::incrementAccount()
{
    for (int i=0;i<10000000;i++)
    {
        client_account.deposit(10);
		/*
         try {
         sleep(10);
         } catch (InterruptedException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
         }*/
    }
}

void Depositor::run()
{
    //std::thread t(this->incrementAccount());
    //t.join();
}

Withdrawer::Withdrawer(Client that_client)
:client_account(that_client)
{
    
}

void Withdrawer::decrementAccount()
{
    for (int i=0;i<10000000;i++)
    {
        client_account.withdraw(10);
		/*
         try {
         sleep(10);
         } catch (InterruptedException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
         }*/
    }
}

void Withdrawer::run()
{
    //std:: thread t(this->decrementAccount());
    //t.join();
}
