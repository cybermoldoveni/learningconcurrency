//
//  Accounting.h
//  LearningConcurrency
//
//  Created by Denis Grigor on 2/6/2014.
//  Copyright (c) 2014 CyberMoldoveni. All rights reserved.
//

#ifndef __LearningConcurrency__Accounting__
#define __LearningConcurrency__Accounting__

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <iomanip>

void AccountTestCase();



class Client
{
    friend std::ostream & operator<<(std::ostream &os, const Client &ciudik);
public:
    Client(int number, std::string name, double balance);
    void deposit(double amount);
    void withdraw(double amount);
private:
    int account_number;
    std::string client_name;
    double balance;
};

class Depositor
{
public:
    Depositor(Client);
    void run();

private:
    Client client_account;
    void incrementAccount();
    
};

class Withdrawer
{
public:
    Withdrawer(Client);
    void run();
private:
    Client client_account;
    void decrementAccount();
    
};





#endif /* defined(__LearningConcurrency__Accounting__) */
